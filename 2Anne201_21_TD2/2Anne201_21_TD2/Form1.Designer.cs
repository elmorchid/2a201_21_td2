﻿
namespace _2Anne201_21_TD2
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.com_Categorie = new System.Windows.Forms.ComboBox();
            this.txt_Prix = new System.Windows.Forms.TextBox();
            this.txt_Intitule = new System.Windows.Forms.TextBox();
            this.txt_Reference = new System.Windows.Forms.TextBox();
            this.lbl_Prix = new System.Windows.Forms.Label();
            this.lbl_Categorie = new System.Windows.Forms.Label();
            this.lbl_Intitule = new System.Windows.Forms.Label();
            this.lbl_Reference = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_Quitter = new System.Windows.Forms.Button();
            this.btn_Supprimer = new System.Windows.Forms.Button();
            this.btn_Modifier = new System.Windows.Forms.Button();
            this.btn_Ajouter = new System.Windows.Forms.Button();
            this.btn_Rechercher = new System.Windows.Forms.Button();
            this.btn_Nouveau = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.com_Categorie);
            this.groupBox1.Controls.Add(this.txt_Prix);
            this.groupBox1.Controls.Add(this.txt_Intitule);
            this.groupBox1.Controls.Add(this.txt_Reference);
            this.groupBox1.Controls.Add(this.lbl_Prix);
            this.groupBox1.Controls.Add(this.lbl_Categorie);
            this.groupBox1.Controls.Add(this.lbl_Intitule);
            this.groupBox1.Controls.Add(this.lbl_Reference);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 217);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Produit";
            // 
            // com_Categorie
            // 
            this.com_Categorie.FormattingEnabled = true;
            this.com_Categorie.Items.AddRange(new object[] {
            "cat1",
            "cat2",
            "cat3"});
            this.com_Categorie.Location = new System.Drawing.Point(135, 125);
            this.com_Categorie.Name = "com_Categorie";
            this.com_Categorie.Size = new System.Drawing.Size(100, 23);
            this.com_Categorie.TabIndex = 8;
            // 
            // txt_Prix
            // 
            this.txt_Prix.Location = new System.Drawing.Point(135, 180);
            this.txt_Prix.Name = "txt_Prix";
            this.txt_Prix.Size = new System.Drawing.Size(100, 21);
            this.txt_Prix.TabIndex = 6;
            // 
            // txt_Intitule
            // 
            this.txt_Intitule.Location = new System.Drawing.Point(135, 78);
            this.txt_Intitule.Name = "txt_Intitule";
            this.txt_Intitule.Size = new System.Drawing.Size(100, 21);
            this.txt_Intitule.TabIndex = 5;
            // 
            // txt_Reference
            // 
            this.txt_Reference.Location = new System.Drawing.Point(135, 30);
            this.txt_Reference.Name = "txt_Reference";
            this.txt_Reference.Size = new System.Drawing.Size(100, 21);
            this.txt_Reference.TabIndex = 4;
            // 
            // lbl_Prix
            // 
            this.lbl_Prix.AutoSize = true;
            this.lbl_Prix.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lbl_Prix.Location = new System.Drawing.Point(16, 186);
            this.lbl_Prix.Name = "lbl_Prix";
            this.lbl_Prix.Size = new System.Drawing.Size(80, 15);
            this.lbl_Prix.TabIndex = 3;
            this.lbl_Prix.Text = "Prix Vente :";
            // 
            // lbl_Categorie
            // 
            this.lbl_Categorie.AutoSize = true;
            this.lbl_Categorie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lbl_Categorie.Location = new System.Drawing.Point(16, 128);
            this.lbl_Categorie.Name = "lbl_Categorie";
            this.lbl_Categorie.Size = new System.Drawing.Size(77, 15);
            this.lbl_Categorie.TabIndex = 2;
            this.lbl_Categorie.Text = "Categorie :";
            // 
            // lbl_Intitule
            // 
            this.lbl_Intitule.AutoSize = true;
            this.lbl_Intitule.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lbl_Intitule.Location = new System.Drawing.Point(16, 81);
            this.lbl_Intitule.Name = "lbl_Intitule";
            this.lbl_Intitule.Size = new System.Drawing.Size(59, 15);
            this.lbl_Intitule.TabIndex = 1;
            this.lbl_Intitule.Text = "Intitule :";
            // 
            // lbl_Reference
            // 
            this.lbl_Reference.AutoSize = true;
            this.lbl_Reference.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lbl_Reference.Location = new System.Drawing.Point(16, 30);
            this.lbl_Reference.Name = "lbl_Reference";
            this.lbl_Reference.Size = new System.Drawing.Size(81, 15);
            this.lbl_Reference.TabIndex = 0;
            this.lbl_Reference.Text = "Reference :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_Quitter);
            this.groupBox2.Controls.Add(this.btn_Supprimer);
            this.groupBox2.Controls.Add(this.btn_Modifier);
            this.groupBox2.Controls.Add(this.btn_Ajouter);
            this.groupBox2.Controls.Add(this.btn_Rechercher);
            this.groupBox2.Controls.Add(this.btn_Nouveau);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(448, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(295, 217);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Operations";
            // 
            // btn_Quitter
            // 
            this.btn_Quitter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Quitter.Location = new System.Drawing.Point(102, 162);
            this.btn_Quitter.Name = "btn_Quitter";
            this.btn_Quitter.Size = new System.Drawing.Size(92, 23);
            this.btn_Quitter.TabIndex = 5;
            this.btn_Quitter.Text = "Quitter";
            this.btn_Quitter.UseVisualStyleBackColor = true;
            this.btn_Quitter.Click += new System.EventHandler(this.btn_Quitter_Click);
            // 
            // btn_Supprimer
            // 
            this.btn_Supprimer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Supprimer.Location = new System.Drawing.Point(102, 133);
            this.btn_Supprimer.Name = "btn_Supprimer";
            this.btn_Supprimer.Size = new System.Drawing.Size(92, 23);
            this.btn_Supprimer.TabIndex = 4;
            this.btn_Supprimer.Text = "Supprimer";
            this.btn_Supprimer.UseVisualStyleBackColor = true;
            this.btn_Supprimer.Click += new System.EventHandler(this.btn_Supprimer_Click);
            // 
            // btn_Modifier
            // 
            this.btn_Modifier.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Modifier.Location = new System.Drawing.Point(102, 104);
            this.btn_Modifier.Name = "btn_Modifier";
            this.btn_Modifier.Size = new System.Drawing.Size(92, 23);
            this.btn_Modifier.TabIndex = 3;
            this.btn_Modifier.Text = "Modifier";
            this.btn_Modifier.UseVisualStyleBackColor = true;
            this.btn_Modifier.Click += new System.EventHandler(this.btn_Modifier_Click);
            // 
            // btn_Ajouter
            // 
            this.btn_Ajouter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Ajouter.Location = new System.Drawing.Point(102, 75);
            this.btn_Ajouter.Name = "btn_Ajouter";
            this.btn_Ajouter.Size = new System.Drawing.Size(92, 23);
            this.btn_Ajouter.TabIndex = 2;
            this.btn_Ajouter.Text = "Ajouter";
            this.btn_Ajouter.UseVisualStyleBackColor = true;
            this.btn_Ajouter.Click += new System.EventHandler(this.btn_Ajouter_Click);
            // 
            // btn_Rechercher
            // 
            this.btn_Rechercher.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Rechercher.Location = new System.Drawing.Point(102, 46);
            this.btn_Rechercher.Name = "btn_Rechercher";
            this.btn_Rechercher.Size = new System.Drawing.Size(92, 23);
            this.btn_Rechercher.TabIndex = 1;
            this.btn_Rechercher.Text = "Rechercher";
            this.btn_Rechercher.UseVisualStyleBackColor = true;
            this.btn_Rechercher.Click += new System.EventHandler(this.btn_Rechercher_Click);
            // 
            // btn_Nouveau
            // 
            this.btn_Nouveau.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Nouveau.Location = new System.Drawing.Point(102, 17);
            this.btn_Nouveau.Name = "btn_Nouveau";
            this.btn_Nouveau.Size = new System.Drawing.Size(92, 23);
            this.btn_Nouveau.TabIndex = 0;
            this.btn_Nouveau.Text = "Nouveau";
            this.btn_Nouveau.UseVisualStyleBackColor = true;
            this.btn_Nouveau.Click += new System.EventHandler(this.btn_Nouveau_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 235);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(731, 81);
            this.dataGridView1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Mise a jour des Produits";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox com_Categorie;
        private System.Windows.Forms.TextBox txt_Prix;
        private System.Windows.Forms.TextBox txt_Intitule;
        private System.Windows.Forms.TextBox txt_Reference;
        private System.Windows.Forms.Label lbl_Prix;
        private System.Windows.Forms.Label lbl_Categorie;
        private System.Windows.Forms.Label lbl_Intitule;
        private System.Windows.Forms.Label lbl_Reference;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_Quitter;
        private System.Windows.Forms.Button btn_Supprimer;
        private System.Windows.Forms.Button btn_Modifier;
        private System.Windows.Forms.Button btn_Ajouter;
        private System.Windows.Forms.Button btn_Rechercher;
        private System.Windows.Forms.Button btn_Nouveau;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

