﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace _2Anne201_21_TD2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public void VIDER(Control f)
        {
            foreach (Control ct in f.Controls)
            {
                if (ct.GetType() == typeof(TextBox) || ct.GetType() == typeof(ComboBox))
                {
                    ct.Text = "";

                }

                if (ct.Controls.Count != 0)
                {
                    VIDER(ct);
                }
            }
        }
        Ado d = new Ado();

        private void Form1_Load(object sender, EventArgs e)
        {
            d = new Ado();
            d.Connecter();
            RemplirGri();
            d.cmd.CommandText = "select * from Produit";
            d.cmd.Connection = d.con;
            d.dr = d.cmd.ExecuteReader();
            d.dt.Load(d.dr);
            dataGridView1.DataSource = d.dt;
            d.dr.Close();
        }

        private void btn_Quitter_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous quitter?", "Configuration", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                d.Deconnecter();
                this.Close();
            }
        }
        //methode Ajouter
        //methode nombre

        //remplissage de dataGridView

        public void RemplirGri()

        {
            if (d.dt.Rows != null)
              {
               d.dt.Clear();
           }

            d.cmd.CommandText = "select * from Produit";
            d.cmd.Connection = d.con;
            d.dr = d.cmd.ExecuteReader();
            d.dt.Load(d.dr);
            dataGridView1.DataSource = d.dt;
            d.dr.Close();
        }
        public int nombre()
        {
            int cpt;
            d.cmd.CommandText = "select Count(Reference) from Produit where Reference='" + txt_Reference.Text + "'";
            d.cmd.Connection = d.con;
            d.Connecter();
            cpt = (int)d.cmd.ExecuteScalar();
            return cpt;
        }
        //Methode Ajouter
        public bool Ajouter()
        {
            if (nombre() == 0)
            {
                d.cmd.CommandText = "insert into Produit values('" + txt_Reference.Text + "','" + txt_Intitule.Text + "','" + com_Categorie.Text + "','" + txt_Prix.Text + "')";
                d.cmd.Connection = d.con;
                d.cmd.ExecuteNonQuery();
                return true;
            }
            return false;
        }       
        
        //mthode supprimer
        public bool SUPPRIMER()
        {
           
            if (nombre() != 0)
            {
                d.cmd.CommandText = "delete from Produit where Reference ='" + txt_Reference.Text + "'";
                d.cmd.Connection = d.con;
                d.cmd.ExecuteNonQuery();
                return true;
            }
            return false;
           
        }
        //Methode modifier
        public bool Modifier()
        {
            if (nombre() != 0)
            {
                d.cmd.CommandText = "update Produit set Intitule ='" + txt_Intitule.Text + "' ,Categorie ='" + com_Categorie.Text + "',Prix ='" + txt_Prix.Text + "' where Reference ='" + txt_Reference.Text + "'";
                d.cmd.Connection = d.con;
                d.cmd.ExecuteNonQuery();
                return true;
            }
            return false;
        }


        private void btn_Ajouter_Click(object sender, EventArgs e)
        {
            if (txt_Reference.Text == "" || txt_Intitule.Text == "" || com_Categorie.Text == "" || txt_Prix.Text == "")
            {
                MessageBox.Show("Voulez-vous remplir tout les champs");
                return;
            }
            if (Ajouter() == true)
            {
                MessageBox.Show("Produit est ajouter avec succes");
                RemplirGri();
            }
            else
            {
                MessageBox.Show("Produit existe deja");
            }
        }

        private void btn_Nouveau_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("voulez vous vider les champs !", "confirmer ", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                VIDER(this);
            }
        }

        private void btn_Supprimer_Click(object sender, EventArgs e)
        {
            if (txt_Reference.Text == "")
            {
                MessageBox.Show("merci de remplir tout les champs");
            }
            if (SUPPRIMER() == true)
            {
                MessageBox.Show("Produit est supprimer avec succee ");
                RemplirGri();

            }
            else
            {
                MessageBox.Show(" Produit n'existe deja");
            }
        }

        private void btn_Modifier_Click(object sender, EventArgs e)
        {
            if (txt_Reference.Text == "" || txt_Intitule.Text == "" || com_Categorie.Text == "" || txt_Prix.Text == "")
            {
                MessageBox.Show("merci de remplir tout les champs");
            }
            if (Modifier() == true)
            {
                MessageBox.Show(" Produit est modifierbien succee ");
                RemplirGri();

            }
            else
            {
                MessageBox.Show("Produit existe pas");
            }
        }

        private void btn_Rechercher_Click(object sender, EventArgs e)
        {
            d.Connecter();
            d.cmd.CommandText = "select Intitule,Categorie,Prix from Produit where Reference = " + txt_Reference.Text + "";
            d.dr = d.cmd.ExecuteReader();
            d.dr.Read();
            txt_Intitule.Text = d.dr[0].ToString();
           com_Categorie.Text = d.dr[1].ToString();
            txt_Prix.Text = d.dr[2].ToString();
            d.Deconnecter();

        }
    }
}
